import { defineConfig } from 'vite'

export default defineConfig({
  base: '',
  build: {
    target: 'modules',
    outDir: './public',
  },
  optimizeDeps:
  {
    esbuildOptions: { target: 'esnext' },
  },
  esbuild: {
    supported: {
      'top-level-await': true
    },
  }
});
