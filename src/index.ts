import { initializeMenu, closeMenu } from './script/menu';
import { initializeNumpad } from './script/numpad';
//import { templatedSlip44Wallet } from './script/wallet';
import { initializeElectrumApplication, electrumClients, electrumEvents } from '@electrum-cash/application';
//import { OracleData, OracleNetwork } from '@generalprotocols/price-oracle';

// Import NFC functions
// import { nfcEvents, deviceSupportsNFC, deviceAllowsNFC, requestAccessToNFC, readFromTag, listenForTags } from 'webnfc';
// import { writeBullBackup, parseBullBackup } from './script/nfc/bchbull.ts';

import moment from 'moment';

export const showView = async function(viewName: string): Promise<void>
{
	const applicationViews = document.querySelectorAll('main > div') as NodeListOf<HTMLElement>;

	for(const view of applicationViews)
	{
		if(view.id === viewName)
		{
			view.style.display = 'flex';
		}
		else
		{
			view.style.display = 'none';
		}
	}

	await closeMenu();
}

export const networkIsConnected = async function(): Promise<boolean>
{
	// assume we are disconnected by default.
	let atLeastOneIsConnected = false;

	// For each client that is connected, update the state.
	for(const electrumClient of electrumClients.values())
	{
		if(electrumClient.status === 1)
		{
			atLeastOneIsConnected = (atLeastOneIsConnected || true);
		}
	}

	// Return connected if at least one server is available.
	if(atLeastOneIsConnected)
	{
		return true;
	}

	// Return disconnected if all servers are unavailable.
	return false;
}

export const networkIsPending = async function(): Promise<boolean>
{
	// assume we are disconnected by default.
	let atLeastOneIsPending = false;

	// For each client that is connected, update the state.
	for(const electrumClient of electrumClients.values())
	{
		if(electrumClient.status > 1)
		{
			atLeastOneIsPending = (atLeastOneIsPending || true);
		}
	}

	// Return connected if at least one server is available.
	if(atLeastOneIsPending)
	{
		return true;
	}

	// Return disconnected if all servers are unavailable.
	return false;
}

export const networkIsSynchronized = async function(): Promise<boolean>
{
	// Fetch the network indicator elements.
	const networkLoadingPercentageElement = document.querySelector('#chainPercent') as HTMLElement;

	return (networkLoadingPercentageElement.innerHTML === '100.0%');
}

export const updateNetworkIndicator = async function(): Promise<void>
{
	// Fetch the network indicator element.
	const networkIndicator = document.querySelector('#chainState') as HTMLElement;
	const loadingIndicator = document.querySelector('#loadingIndicator') as HTMLElement;

	// Initialize an unknown network state.
	let updatedNetworkState = 'unknown';

	if(await networkIsPending())
	{
		updatedNetworkState = 'connecting';
	}
	else if(!await networkIsConnected())
	{
		updatedNetworkState = 'disconnected';
	}
	else if(!await networkIsSynchronized())
	{
		updatedNetworkState = 'downloading';
	}
	else
	{
		updatedNetworkState = 'connected';
	}

	// Update the status if it has changed.
	if(loadingIndicator.style.animationName !== updatedNetworkState)
	{
		loadingIndicator.style.animationName = updatedNetworkState;
	}

	// Update the status if it has changed.
	if(networkIndicator.innerHTML !== updatedNetworkState)
	{
		networkIndicator.innerHTML = updatedNetworkState;
	}
}

export class Application
{
	public storeName = 'StuffedCookieLand';
	public extendedPublicKey = 'xpub661MyMwAqRbcFN5cYjvBDYSGNHg9ZbG1TSmrSmWfd6fvB4DtnLLiVJckLkMAkAs9RAddWGXmM5FD6sAgSt5LJvH7AAzoYEoZXX4hqU2LpkM';

	public networkWarning = undefined;

	// Determines if we should play sound effects or not.
	public enableSound = true;

	// Determines which colors and images to use as a theme.
	public theme: string;

	// PIN code required to enable settings.
	// private settingsPIN: number;

	// Determines if the settings are currently locked or unlocked.
	public settingsLock: boolean;

	constructor()
	{
		initializeMenu();
		initializeNumpad();

		// ...
		this.initializeElectrum();

		// Listen for relayed messages and handle them with the callback handler.
		// OracleNetwork.listenToRelays(this.updateOracleState);

		// document.getElementById('enableNotifications').addEventListener('click', this.enableNotifications.bind(this));
		document.getElementById('headerStatusIcon').addEventListener('click', showView.bind(this, 'viewConnectionStatus'));
		document.getElementById('menuNewPayment').addEventListener('click', showView.bind(this, 'viewPaymentInput'));
		document.getElementById('menuNFC').addEventListener('click', showView.bind(this, 'viewNFC'));
		document.getElementById('menuSLIP44').addEventListener('click', showView.bind(this, 'viewSLIP44'));

		showView('viewConnectionStatus');

		//this.initializeNFC();

		// TODO: initialize a wallet.
		// this.wallet = new templatedSlip44Wallet('xpub6C4sQ8E2yoXeTQd1jce61PYtyF2c5WLzv85Ndy3bDsdwz3oFTaZFm9VdFBQ58Mfov9gVUVeNLtjcEEWymGrrSBZWNB9XSghFXemtR3gYYQN');

		this.testStorageAPI();
	}

	async testStorageAPI()
	{
		console.log(await navigator.storage.estimate());
	}

	/*
	async initializeNFC()
	{
		// Show/hide some stuff.
		await this.updateDisplay();
		await this.setState('inactive');

		// Hook up.
		nfcEvents.addListener('TagRecords', this.readBackup);

		nfcEvents.addListener('ReadStart', this.setState.bind(null, 'reading tag'));
		nfcEvents.addListener('ReadStart', this.logMessage.bind(null, 'ReadStart'));

		nfcEvents.addListener('ReadStop', this.setState.bind(null, 'inactive'));
		nfcEvents.addListener('ReadStop', this.logMessage.bind(null, 'ReadStop'));

		nfcEvents.addListener('WriteStart', this.setState.bind(null, 'writing tag'));
		nfcEvents.addListener('WriteStart', this.logMessage.bind(null, 'WriteStart'));

		nfcEvents.addListener('WriteStop', this.setState.bind(null, 'inactive'));
		nfcEvents.addListener('WriteStop', this.logMessage.bind(null, 'WriteStop'));

		nfcEvents.addListener('PermissionGranted', this.updateDisplay);
		nfcEvents.addListener('PermissionDenied', this.updateDisplay);

		// Hook up permission request.
		document.getElementById('permNFC').addEventListener('click', requestAccessToNFC);

		// Hook up tag reading.
		document.getElementById('readNFC').addEventListener('click', readFromTag.bind(null, 5));
		document.getElementById('listenNFC').addEventListener('click', listenForTags.bind(null, 0, false));

		// Hook up tag writing.
		document.getElementById('writeNFC1').addEventListener('click', writeBullBackup.bind(null, 'TestingKey', false, 5));
		document.getElementById('writeNFC2').addEventListener('click', writeBullBackup.bind(null, 'TestingKey', true, 5));
	}
	*/

	async setState(message: string)
	{
		document.getElementById('stateNFC').innerHTML = message;
	}

	async logMessage(signal: string, message: string = '')
	{
		document.getElementById('log').innerHTML += `${signal}: ${message} <br/>`;
	}

	async updateDisplay()
	{
		/*
		document.getElementById('hasNFC').innerHTML = JSON.stringify(await deviceSupportsNFC());
		document.getElementById('allowNFC').innerHTML = JSON.stringify(await deviceAllowsNFC());

		if(!await deviceAllowsNFC())
		{
			document.getElementById('permNFCLine').style.display = 'flex';
			document.getElementById('writeNFCLine').style.display = 'none';
			document.getElementById('writeNFCLine').style.display = 'none';
			document.getElementById('listenNFCLine').style.display = 'none';
		}
		else
		{
			document.getElementById('permNFCLine').style.display = 'none';
			document.getElementById('readNFCLine').style.display = 'flex';
			document.getElementById('writeNFCLine').style.display = 'flex';
			document.getElementById('listenNFCLine').style.display = 'flex';
		}
		*/
	}

	/*
	async readBackup(records)
	{
		try
		{
			const backupKey = await parseBullBackup(records);

			document.getElementById('log').innerHTML += `Read BCH Bull backup: ${backupKey}<br/>`;
		}
		catch(error)
		{
			document.getElementById('log').innerHTML += `Read unknown tag: ${error}<br/>`;
		}
	}
	*/

	async initializeElectrum()
	{
		electrumEvents.on('ChainStatus', this.updateChainState.bind(this));

		electrumEvents.on('connecting', this.updateElectrumServerStatus.bind(this, 'connecting'));
		electrumEvents.on('connected', this.updateElectrumServerStatus.bind(this, 'connected'));
		electrumEvents.on('reconnecting', this.updateElectrumServerStatus.bind(this, 'connecting'));
		electrumEvents.on('disconnecting', this.updateElectrumServerStatus.bind(this, 'connecting'));
		electrumEvents.on('disconnected', this.updateElectrumServerStatus.bind(this, 'disconnected'));

		electrumEvents.on('disconnected', this.updateElectrumServerStatus.bind(this, 'loading'));

		await initializeElectrumApplication('Experience Cash - Point of Sale');

		// Set initial server status.
		await this.updateElectrumServerStatus();

		// Trigger an additional update manually.
		// NOTE: This is needed because we get chain update events in aggregate, but need to display server block heights that updates inbetween.
		setTimeout(this.updateElectrumServerStatus.bind(this), 250);
	}

	async updateElectrumServerStatus()
	{
		const serverTemplate = document.querySelector('#serverListEntry') as HTMLTemplateElement;
		const serverList = document.querySelector('#electrumServers') as HTMLElement;

		// Update the chain state status indicator.
		await updateNetworkIndicator();

		//
		for(const [ electrumServer, electrumClient ] of electrumClients)
		{
			// Determine a valid identifier for the server name.
			const electrumServerName = electrumServer.replace(new RegExp('.', 'g'), '_');

			// Determine the connection status color to use.
			const statusColor = (electrumClient.status > 1 ? 'orange' : (electrumClient.status < 1 ? 'red' : 'green'));
			const statusTooltip = (electrumClient.status > 1 ? 'pending' : (electrumClient.status < 1 ? 'disconnected' : 'connected'));

			// Try to get an existing entry for this server.
			const existingServerListEntry = serverList.querySelector(`#${electrumServerName}`);

			// The server does not exist in the list..
			if(!existingServerListEntry)
			{
				// Clone a server list entry.
				const newServerListEntry = serverTemplate.content.cloneNode(true) as HTMLElement;

				// Set a unique ID on this entry.
				newServerListEntry.querySelector('li').setAttribute('id', electrumServerName);

				// Add the server details to the page.
				serverList.appendChild(newServerListEntry);
			}

			// Update the server details.
			serverList.querySelector(`#${electrumServerName} .title`).innerHTML = electrumClient.hostIdentifier;
			serverList.querySelector(`#${electrumServerName} .subtitle`).innerHTML = electrumClient.software + ', at #' + electrumClient.chainHeight;
			serverList.querySelector(`#${electrumServerName} .subtitle`).setAttribute('title', moment(electrumClient.lastReceivedTimestamp).toISOString());
			serverList.querySelector(`#${electrumServerName} .status`).setAttribute('style', `color: ${statusColor}`);
			serverList.querySelector(`#${electrumServerName} .status`).setAttribute('title', statusTooltip);
		}
	}

	async updateChainState(status)
	{
		document.querySelector('#chainPercent').innerHTML = `${status.verifiedPercent}%`;
		document.querySelector('#chainHeight').innerHTML = `(#${status.verifiedHeight})`;

		await updateNetworkIndicator();
		await this.updateElectrumServerStatus();

		if(status.verifiedPercent === "100.0")
		{
			if(Notification.permission === 'granted')
			{
				// Create an example notification:
				new Notification('New block height', { body: `We are currently on #${status.verifiedHeight}`, icon: './src/images/logo.png', tag: 'Chain Height' });
			}
		}
	}

	async enableNotifications()
	{
		// Do nothing if notifications are unavailable.
		if (!("Notification" in window))
		{
			return;
		}

		// Request permission is necessary.
		if(Notification.permission !== "granted")
		{
			await Notification.requestPermission();
		}

		this.updateNotificationState();
	}

	async updateNotificationState()
	{
		// Do nothing if notifications are unavailable.
		if (!("Notification" in window))
		{
			document.querySelector('#enableNotifications .subtitle').innerHTML = 'Unavailable';
		}

		// Request permission is necessary.
		else if(Notification.permission === "granted")
		{
			document.querySelector('#enableNotifications .subtitle').innerHTML = 'Enabled';
		}

		else
		{
			document.querySelector('#enableNotifications .subtitle').innerHTML = 'Disabled';
		}
	}

	/*
	async updateOracleState(_topic, content)
	{
		// Extract the components of the message.
		const { message, publicKey, signature } = content;

		// Verify the signature.
		const signatureValidationResult = await OracleData.verifyMessageSignature(message, signature, publicKey)

		// Ignore messages that do not provide a valid signature.
		if(!signatureValidationResult)
		{
			console.log('WTF');
			return;
		}

		// Ignore non-price messages.
		if(!await OracleData.isDataMessage(message))
		{
			return;
		}

		// Extract the timestamp and value from the price message. (messageTimestamp)
		const { priceValue } = await OracleData.parsePriceMessage(message);

		document.getElementById('headerTitle').innerHTML = `${priceValue}/BCH`;
	}
	*/
}

// Create an instance of the application.
new Application();
