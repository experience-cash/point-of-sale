// Install service worker when set up as a PWA/TWA.
if('serviceWorker' in navigator)
{
	navigator.serviceWorker.register(new URL('./serviceWorker.ts', import.meta.url), {type: 'module'});
}

// Set a name for the local cache.
// NOTE: This is required as the cache system allows for multiple caches.
const cacheName = 'sw-cache';

// Set up a list of files that we will cache.
const cacheFiles =
[
	'/index.html',
];

// Set up a list of data types that we will cache.
const cacheTypes =
[
	'audio',
	'font',
	'image',
	'script',
	'style',
	'video',
];

// Set up a handler for fetch requests.
const handleFetchRequest = async function(event)
{
	// Determine if this file should be cached.
	const shouldCache = (cacheFiles.includes(event.request.url) || cacheTypes.includes(event.request.destination));

	// Do nothing if this request should not be cached.
	if(!shouldCache)
	{
		return;
	}

	// Open the local cache.
	const cache = await caches.open(cacheName);

	// Check if the request exist in the local cache.
	const cachedResponse = await cache.match(event.request);

	// Return the response from cache if available.
	if(cachedResponse)
	{
		event.respondWith(cachedResponse);
	}

	// Fetch the resource from the network, even if it was already cache.
	const fetchedResponse = await fetch(event.request);

	// Store the up to date resource in the cache for the future.
	cache.put(event.request, fetchedResponse.clone());

	// Respond with the resource fetched from the network, if one from cache has not already been used.
	if(!cachedResponse)
	{
		event.respondWith(fetchedResponse);
	}

}

// Set up an install script to install the core application files.
const cacheWebApplication = async function()
{
	// Open the local cache.
	const cache = await caches.open(cacheName);

	// Cache the application files.
	for(const cacheFileURL of cacheFiles)
	{
		cache.add(cacheFileURL)
	}

	return true;
}

// Wrapper that caches the web application files.
const installWebApplication = function(event)
{
	event.waitUntil(cacheWebApplication);
}

// Set up a event listeners to install the web application.
self.addEventListener('install', installWebApplication);
self.addEventListener('fetch', handleFetchRequest);

