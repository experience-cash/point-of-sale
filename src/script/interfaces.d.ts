// Define blockheight to be a number.
export type BlockHeight = number;

// Define Satoshis to be a number.
export type Satoshis = number;

// Define a public key to be a string.
export type PublicKey = string;

// Define an address to be a string.
export type Address = string;

// Define an extended public key to be a string.
export type ExtendedPublicKey = string;

// Define a transaction has to be a string.
export type TransactionHash = string;

// ...
export type TransactionList = Map<TransactionHash, TransactionData>;

// ...
export type AddressHistory = Array<TransactionData>;

// ...
export type AddressList = Array<Address>

export type ChangeAddressList = Record<number, Address>;
export type DefaultAddressList = Record<number, Address>;

//
export type DerivationType = number;
export type DerivationIndex = number;

// Define storage for extended public key derived address histories.
export type ExtendedAddressHistory = Record<DerivationType, Record<DerivationIndex, AddressHistory>>;

export interface TransactionData
{
	height: BlockHeight,
	tx_hash: TransactionHash,
	received?: Satoshis,
}

export interface ExtendedPublicKeyAddressList
{
	changeIndex: number;
	addressIndex: number;

	addressList: Array<string>;
}
