import { writeToTag } from 'webnfc';
import type { NDEFRecord } from 'webnfc';

// Set up text encoding and decoding capabilities.
const textEncoder = new TextEncoder();
const textDecoder = new TextDecoder();

export const writeBullBackup = async function(backupData: string, overwrite: boolean = false, timeoutInSeconds?: number): Promise<void>
{
	return writeToTag('backup/bchbull', textEncoder.encode(backupData), overwrite, timeoutInSeconds);
}

export const parseBullBackup = async function(records: Array<NDEFRecord>): Promise<string>
{
	// Throw an error if the tag has an unexpected number of records.
	if(records.length !== 1)
	{
		throw(new Error(`Parsed tag has the wrong number of records (${records.length}), expected '1'`));
	}

	// Extract the only record for legibility.
	const [ firstRecord ] = records;

	// Throw an error if the tag has the wrong mime type.
	if(firstRecord.mediaType !== "backup/bchbull")
	{
		throw(new Error(`Parsed tag has the wrong mime type (${firstRecord.mediaType}), expected 'backup/bchbull'.`));
	}

	try
	{
		// Parse the backup data.
		const backupData = textDecoder.decode(firstRecord.data);

		// Return the parsed backup data.
		return backupData;
	}
	catch(error)
	{
		throw(new Error(`Failed to parse tag as a BCH Bull backup: ${error}`));
	}
}
