// ...
let inputAmount = '0';

const checkoutButton = document.getElementById('checkout') as HTMLButtonElement;
const numpadButtons = document.getElementById('numpad').querySelectorAll('button') as NodeListOf<HTMLButtonElement>;
const amountDisplay = document.getElementById('amount').querySelector('b') as HTMLElement;

export const initializeNumpad = async function(): Promise<void>
{

	// Initial presentation of the amount.
	displayAmount();

	for(const button of numpadButtons)
	{
		if(Number.isNaN(Number(`${button.innerHTML}0`)))
		{
			button.addEventListener('click', shortenAmount);
		}
		else
		{
			button.addEventListener('click', extendAmount);
		}
	}
}

const displayAmount = async function(): Promise<void>
{
	amountDisplay.innerHTML = `${inputAmount}`;

	checkoutButton.disabled = (!await validateAmount() ? true : false);
}

const extendAmount = async function(event: Event): Promise<void>
{
	const target = event.target as HTMLElement;
	const character = target.innerHTML;

	// Replace the initial 0 value when the user first enters a non-decimal point number.
	// NOTE: This also prevents the user from entering multiple leading zeroes, as it will remove the first one every time a new one is added.
	if((inputAmount === '0') && (character !== '.'))
	{
		inputAmount = '';
	}

	// Do nothing if there is already a decimal point.
	if((character === '.') && (inputAmount.indexOf('.') !== -1))
	{
		return;
	}

	// Add the user provided character to the amount.
	inputAmount += character;

	// Update display presentation.
	displayAmount();
}

const shortenAmount = async function(): Promise<void>
{
	inputAmount = inputAmount.slice(0, -1);

	if(inputAmount.length === 0)
	{
		clearAmount();
	}

	// Update display presentation.
	displayAmount();
}

const clearAmount = async function(): Promise<void>
{
	inputAmount = '0';

	// Update display presentation.
	displayAmount();
}

const validateAmount = async function(): Promise<boolean>
{
	// Reject if the last character isn't a number.
	if(Number.isNaN(Number(inputAmount.slice(-1))))
	{
		return false;
	}

	// Reject any data that isn't a finate number.
	if(!Number.isFinite(Number(inputAmount)))
	{
		return false;
	}

	// Reject empty state.
	if(Number(inputAmount) <= 0)
	{
		return false;
	}

	// Accept an finite number.
	return true;
}
