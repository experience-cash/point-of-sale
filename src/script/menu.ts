// cache local references to document nodes.
const menuButton = document.getElementById('menuButton') as HTMLButtonElement;
const menuDialog = document.getElementById('menu') as HTMLDialogElement;


const closeMenuOnDialogClick = function(event: Event)
{
	const target = event.target as HTMLElement;

	if(target.nodeName === 'DIALOG')
	{
		closeMenu();
	}
}

/**
 * Initialize the menu and hook up menu related events.
 */
export const initializeMenu = async function(): Promise<void>
{
	// Hook up the menu button to open the menu.
	menuButton.addEventListener('click', openMenu );

	// Hook up backdrop to close the menu.
	menuDialog.addEventListener('click', closeMenuOnDialogClick);
}

/**
 * Show the menu to the user.
 */
export const openMenu = async function(): Promise<void>
{
	menuDialog.showModal();
	menuDialog.classList.add('open');
}

/**
 * Hide the menu from the user.
 */
export const closeMenu = async function(): Promise<void>
{
	// Do nothing if the menu is already closed.
	if(!menuDialog.classList.contains('open'))
	{
		return;
	}

	menuDialog.classList.remove('open');
	menuDialog.addEventListener('transitionend', cleanupMenu, { capture: true, once:  true, passive: true });
}

/**
 * Close the menu dialog element and clean up event listeners.
 */
const cleanupMenu = async function(): Promise<void>
{
	menuDialog.close();
	//menuDialog.removeEventListener('transitionend', cleanupMenu, false);
}
